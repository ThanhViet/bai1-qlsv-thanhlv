package com.example.demokotlin.view.fragment

import android.app.SearchManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SearchView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.demokotlin.R
import com.example.demokotlin.core.Core
import com.example.demokotlin.model.StudentModel
import com.example.demokotlin.view.adapter.ListAdapter
import com.example.demokotlin.view.base.BaseFragment
import kotlinx.android.synthetic.main.header_layout.*
import kotlinx.android.synthetic.main.list_student_layout.*
import java.util.*

class ListFragment: BaseFragment() {

    private var cloneData: ArrayList<StudentModel> = Core.data.clone() as ArrayList<StudentModel>
    private var adapterStudent: ListAdapter? = null
    private var listener: Listener? = null
    private var optionSort = "--none--"
    private var optionFilter = "--none--"
    private var onDoSort = false
    private var isStart = true

    override fun initView() {
        super.initView()
        tv_headerTitle.text = "QLSV - STUDENTS LIST"

        val searchManager = requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchBox.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
        searchBox.setQuery("", false)
        searchBox.clearFocus()
        rootView.requestFocus()
        searchBox.isFocusable = false
        searchBox.isIconifiedByDefault = false

        val adapterSort = ArrayAdapter.createFromResource(
            requireContext(), R.array.spin_sort,
            R.layout.support_simple_spinner_dropdown_item
        )
        adapterSort.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spin_sort.adapter = adapterSort

        val adapterFilter = ArrayAdapter.createFromResource(
            requireContext(), R.array.spin_filter,
            R.layout.support_simple_spinner_dropdown_item
        )
        adapterFilter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spin_filter.adapter = adapterFilter


    }

    override fun initData() {
        super.initData()
    }

    override fun viewController() {
        super.viewController()

        //back
        btn_back_header.setOnClickListener(View.OnClickListener { onBackClick() })


        //search
        searchBox.setOnQueryTextFocusChangeListener(OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                spin_sort.setSelection(0)
                spin_filter.setSelection(0)
                adapterStudent?.updateData(Core.data)
                option.visibility = View.GONE
            } else {
                option.visibility = View.VISIBLE
                hideSoftKeyBoard()
            }
        })
        searchBox.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                adapterStudent?.filter?.filter(newText)
                return false
            }
        })

        //show list
        adapterStudent = ListAdapter(context, cloneData, listener!!)
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recyclerView.addItemDecoration(DividerItemDecoration(activity,LinearLayoutManager.VERTICAL))
        recyclerView.adapter = adapterStudent


        //spinner
        spin_sort.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            @RequiresApi(api = Build.VERSION_CODES.N)
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                optionSort = adapterView.getItemAtPosition(i).toString()
                onDoSort = true
                doOption(optionSort, optionFilter)
                onDoSort = false
                if (optionSort != "--none--")
                    Toast.makeText(context, optionSort, Toast.LENGTH_LONG).show()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        spin_filter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            @RequiresApi(api = Build.VERSION_CODES.N)
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                optionFilter = adapterView.getItemAtPosition(i).toString()
                doOption(optionSort, optionFilter)
                if (!isStart)
                    Toast.makeText(adapterView.context, optionFilter, Toast.LENGTH_LONG).show()
                isStart = false
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = activity as Listener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        initData()

        return inflater.inflate(R.layout.list_student_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        viewController()
    }

    fun updateRecyclerView(newList: ArrayList<StudentModel>) {
        adapterStudent?.updateData(newList)
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    fun doOption(sort: String, filter: String) {
        searchBox.setQuery("", false)
        searchBox.clearFocus()
        if (sort == "--none--" && onDoSort) return
        cloneData = Core.data.clone() as ArrayList<StudentModel>
        val temp = ArrayList<StudentModel>()
        if (filter == "--All--") {
            temp.addAll(cloneData)
        } else
            for (i in cloneData.indices)
                if (cloneData[i].degree == filter) temp.add(cloneData[i])
            if (sort == "by Name") temp.sortWith(Comparator { o1, o2 ->
                o1.fullName.compareTo(o2.fullName)
            })
            if (sort == "by DOB") temp.sortWith(Comparator { o1, o2 ->
                val result = 0
                if (o1.DOB > o2.DOB) return@Comparator 1
                if (o1.DOB < o2.DOB) -1 else result
            })
            if (sort == "by Phone Number") temp.sortWith(Comparator { o1, o2 ->
                        o1.phoneNumber.compareTo(o2.phoneNumber)
            })
        adapterStudent?.updateData(temp)
    }

    private fun hideSoftKeyBoard() {
        if (view != null) {
            val imm = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(requireView().windowToken, 0)
        }
    }

    private fun onBackClick(){
        activity?.onBackPressed()
    }
}