package com.example.demokotlin.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.demokotlin.R
import com.example.demokotlin.model.StudentModel
import com.example.demokotlin.view.fragment.Listener

class ListAdapter(var
    context: Context?,
    var cloneData: ArrayList<StudentModel>,
    var listener: Listener
) : RecyclerView.Adapter<ListAdapter.AccountViewHolder>(), Filterable {

    private var allList: ArrayList<StudentModel> = ArrayList<StudentModel>(cloneData)
    private var studentList: ArrayList<StudentModel> = ArrayList<StudentModel>(cloneData)

    class AccountViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var fullName: TextView = itemView.findViewById(R.id.tv_name)
        var phoneNumber: TextView = itemView.findViewById(R.id.tv_phone)
        var degree: TextView = itemView.findViewById(R.id.tv_degree)
        var btnEdit: LinearLayout = itemView.findViewById(R.id.ln_img_edit)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_student_list, parent, false)

        return AccountViewHolder(view)
    }

    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {

        val item: StudentModel = studentList[position]
        holder.fullName.text = item.fullName
        holder.phoneNumber.text = item.phoneNumber
        holder.degree.text = item.degree
        holder.btnEdit.setOnClickListener { listener.sendToDetail(item) }
    }

    override fun getItemCount(): Int {
        return studentList.size
    }

    override fun getFilter(): Filter? {
        return listFilter
    }

    private val listFilter: Filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence): FilterResults {
            val filteredList: MutableList<StudentModel> = java.util.ArrayList()
            if (constraint == null || constraint.isEmpty()) {
                filteredList.addAll(allList)
            } else {
                val filterPattern = constraint.toString().toLowerCase().trim { it <= ' ' }
                for (item in allList) {

                    //filter by Name
                    if (item.fullName.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item)
                    }
                    //filter by DOB
                    val date: String = item.DOB.toString()
                    if (date.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item)
                    }
                    //filter by Phone
                    if (item.phoneNumber.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item)
                    }
                }
            }
            val results = FilterResults()
            results.values = filteredList
            return results
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            studentList.clear()
            studentList.addAll(results.values as ArrayList<StudentModel>)
            notifyDataSetChanged()
        }
    }

    fun updateData(newList: java.util.ArrayList<StudentModel>) {
        studentList.clear()
        studentList.addAll(newList)
        allList = ArrayList<StudentModel>(studentList)
        notifyDataSetChanged()
    }
}

