package com.example.demokotlin.view.fragment

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.example.demokotlin.R
import com.example.demokotlin.core.Core
import com.example.demokotlin.model.StudentModel
import com.example.demokotlin.view.base.BaseFragment
import kotlinx.android.synthetic.main.edit_student_layout.*
import kotlinx.android.synthetic.main.header_layout.*

class AddFragment: BaseFragment() {
    private var listener: Listener? = null

    private var currentStudent: StudentModel? = null
    private var newStudent: StudentModel? = null
    private var degree = ""


    private var isNothing = false

    override fun initData() {
        super.initData()
        currentStudent = requireArguments()["key-student"] as StudentModel?
        println("thanh dep trai $currentStudent")
    }

    override fun initView() {
        super.initView()
        edt_DOB.inputType = InputType.TYPE_CLASS_NUMBER
        edt_phoneNumber.inputType = InputType.TYPE_CLASS_PHONE
    }



    override fun viewController() {
        super.viewController()

        val adapter = ArrayAdapter.createFromResource(
            requireContext(), R.array.spin_degree, R.layout.support_simple_spinner_dropdown_item
        )
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spin_degree.adapter = adapter
        btn_back_header?.setOnClickListener(View.OnClickListener { onBackClick() })

        //show Data
        if (currentStudent == null) {
            tv_headerTitle.text = "QLSV - ADD STUDENT"
            btn_submit.text = "ADD>>"
            btn_delete.visibility = View.GONE
        } else {
            tv_headerTitle.text = "QLSV - EDIT STUDENT"
            btn_submit.text = "SAVE>>"
            btn_delete.visibility = View.VISIBLE
            edt_fullName.setText(currentStudent!!.fullName)
            edt_DOB.setText(currentStudent!!.DOB.toString())
            edt_phoneNumber.setText(currentStudent!!.phoneNumber)
            spin_degree.setSelection(if (currentStudent!!.degree == "Cao đẳng") 0 else 1)
        }

        //action Spinner
        spin_degree.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                degree = when (i) {
                    0 -> {
                        "Cao đẳng"
                    }
                    else -> {
                        "Đại học"
                    }
                }
            }
            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }


        //action Delete
        btn_delete.setOnClickListener(View.OnClickListener {
            hideSoftKeyBoard()
            AlertDialog.Builder(context)
                .setTitle("Xóa sinh viên!")
                .setMessage("Bạn muốn xóa sinh viên này?")
                .setPositiveButton(
                    android.R.string.yes
                ) { _, _ -> // Continue with ok
                    Core.data.remove(currentStudent)
                    Toast.makeText(context, "Bạn đã xóa 1 SV!", Toast.LENGTH_LONG).show()
                    listener?.updateRecyclerView(Core.data)
                    onBackClick()
                } // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show()
        })


        //action Submit
        btn_submit?.setOnClickListener(View.OnClickListener {
            hideSoftKeyBoard()
            isNothing = false
            if (checkValidate()) {
                if (currentStudent == null) {
                    newStudent?.let { it -> Core.data.add(0, it) } // add to top list
                    Toast.makeText(context, "Bạn đã thêm 1 SV!", Toast.LENGTH_LONG).show()
                    onBackClick()
                } else {
                    if (isNothing) {
                        Toast.makeText(context, "Nothing changes!", Toast.LENGTH_LONG).show()
                        onBackClick()
                        return@OnClickListener
                    } else {
                        for (i in 0 until Core.data.size) if (Core.data.get(i) === currentStudent) {
                            Core.data.remove(currentStudent)
                            newStudent?.let { it -> Core.data.add(i, it) }
                            listener?.updateRecyclerView(Core.data)
                            break
                        }
                        Toast.makeText(context, "Bạn đã sửa 1 SV!", Toast.LENGTH_LONG).show()
                        onBackClick()
                    }
                }
            }
        })
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = activity as Listener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        initData()

        return inflater.inflate(R.layout.edit_student_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        viewController()
    }

    private fun hideSoftKeyBoard() {
        if (view != null) {
            val imm = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(requireView().windowToken, 0)
        }
    }


    private fun checkValidate(): Boolean {
        if (edt_fullName.text.toString().isEmpty()) {
            Toast.makeText(context, "Vui lòng nhập tên!", Toast.LENGTH_LONG).show()
            edt_fullName.requestFocus()
            return false
        }
        if (edt_DOB.text.toString().isEmpty()) {
            Toast.makeText(requireContext(), "Vui lòng nhập năm sinh!", Toast.LENGTH_LONG).show()
            edt_DOB.requestFocus()
            return false
        }
        var DOB = 0
        DOB = try {
            edt_DOB.text.toString().toInt()
        } catch (e: Exception) {
            Toast.makeText(
                context,
                "Năm sinh sai định dạng.\n Vui lòng nhập lại!",
                Toast.LENGTH_LONG
            ).show()
            edt_DOB.requestFocus()
            return false
        }
        if (edt_phoneNumber.text.toString().isEmpty()) {
            Toast.makeText(requireContext(), "Vui lòng nhập số điện thoại!", Toast.LENGTH_LONG).show()
            edt_phoneNumber.requestFocus()
            return false
        }
        newStudent = StudentModel(
            edt_fullName.text.toString(),
            DOB,
            edt_phoneNumber.text.toString(),
            degree
        )
        if (currentStudent != null) {
            if (newStudent == currentStudent) {
                isNothing = true
                return true
            }
        }
        for (i in 0 until Core.data.size)
            if (Core.data.get(i) != currentStudent
                && edt_phoneNumber.text.toString() == Core.data[i].phoneNumber
            ) {
                Toast.makeText(context,
                    "Số điện thoại đã tồn tại.\n Vui lòng nhập lại!",
                    Toast.LENGTH_LONG).show()
                edt_phoneNumber.requestFocus()
                return false
            }
        return true
    }

    private fun onBackClick(){
        activity?.onBackPressed()
    }
}