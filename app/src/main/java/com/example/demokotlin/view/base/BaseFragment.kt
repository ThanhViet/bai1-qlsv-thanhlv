package com.example.demokotlin.view.base

import androidx.fragment.app.Fragment

open class BaseFragment: Fragment() {
    open fun initData() {}
    open fun initView() {}
    open fun viewController() {}
}