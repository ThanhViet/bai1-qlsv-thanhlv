package com.example.demokotlin.view.fragment

import com.example.demokotlin.model.StudentModel
import java.util.*

interface Listener {
    fun sendToDetail(student: StudentModel?)
    fun updateRecyclerView(studentList: ArrayList<StudentModel>)
}