package com.example.demokotlin

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.demokotlin.model.StudentModel
import com.example.demokotlin.view.fragment.AddFragment
import com.example.demokotlin.view.fragment.ListFragment
import com.example.demokotlin.view.fragment.Listener
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), Listener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn_addStudent.setOnClickListener(View.OnClickListener {
            body.visibility = View.GONE
            val bundle = Bundle()
            bundle.putSerializable("key-student", null)
            supportFragmentManager.beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.fragment_container_view, AddFragment::class.java, bundle)
                .addToBackStack("")
                .commit()
        })


        btn_showList.setOnClickListener(View.OnClickListener {
            body.visibility = View.GONE
            val listStudentFragment = ListFragment()
            supportFragmentManager.beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.fragment_container_view, listStudentFragment, "listFragment")
                .addToBackStack("")
                .commit()
        })

    }

    override fun sendToDetail(student: StudentModel?) {
        val bundle = Bundle()
        bundle.putSerializable("key-student", student)
        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .add(R.id.fragment_container_view, AddFragment::class.java, bundle)
            .addToBackStack("")
            .commit()

    }

    override fun updateRecyclerView(studentList: ArrayList<StudentModel>) {
        val listFrag: ListFragment? = supportFragmentManager
            .findFragmentByTag("listFragment") as ListFragment?
        listFrag?.updateRecyclerView(studentList)
    }

    override fun onBackPressed() {
        body.visibility = View.VISIBLE
        super.onBackPressed()
    }
}
