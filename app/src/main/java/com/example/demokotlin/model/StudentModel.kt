package com.example.demokotlin.model

import java.io.Serializable

data class StudentModel(
    var fullName: String,
    var DOB: Int,
    var phoneNumber: String,
    var degree: String) : Serializable {
}